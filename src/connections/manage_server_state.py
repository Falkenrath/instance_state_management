from botocore.exceptions import ClientError

def shutdown_instances(instance_ids, ec2_client):
    waiter = ec2_client.get_waiter('instance_stopped')
    # stop instance
    try:
        ec2_client.stop_instances(InstanceIds=instance_ids)
    except ClientError as e:
        if e.response['Error']['Code'] == 'InvalidInstanceId.Malformed':
            print(e)
            return e
        else:
            print(e)
            return e
    except Exception as e:
        print(e)
        return e    
    # use waiter
    waiter.wait(InstanceIds=instance_ids)
    # verify response
    return ec2_client.describe_instance_status(InstanceIds=instance_ids)

def start_instances(instance_ids, ec2_client):
    waiter = ec2_client.get_waiter('instance_running')
    # start instance
    try:
        ec2_client.start_instances(InstanceIds=instance_ids)
        
    except ClientError as e:
        if e.response['Error']['Code'] == 'InvalidInstanceId.Malformed':
            print(e)
            return e
        else:
            print(e)
            return e
    except Exception as e:
        print(e)
        return e    
    # use waiter
    waiter.wait(InstanceIds=instance_ids)
    # verify response
    return ec2_client.describe_instance_status(InstanceIds=instance_ids)


def simulate_stop(instance_ids, ec2_client):
    print("Simulate Start Stop")
    waiter = ec2_client.get_waiter('instance_stopped')
    # simulate stop instance
    try:
        ec2_client.stop_instances(InstanceIds=instance_ids, DryRun=True)
    except ClientError as e:
        if e.response['Error']['Code'] == 'DryRunOperation':
            print('Dry Run Operation')
            return(0)
        elif e.response['Error']['Code'] == 'InvalidInstanceId.Malformed':
            print(e)
            return e
        else:
            print(e)
            return e
    except Exception as e:
        print(e)
        return e    
    # use waiter
    waiter.wait(InstanceIds=instance_ids)
    # verify response
    return ec2_client.describe_instance_status(InstanceIds=instance_ids)


def simulate_start(instance_ids, ec2_client):
    waiter = ec2_client.get_waiter('instance_running')
    # simulate stop instance
    try:
        ec2_client.start_instances(InstanceIds=instance_ids, DryRun=True)
        
    except ClientError as e:
        if e.response['Error']['Code'] == 'DryRunOperation':
            print('Dry Run Operation')
            return(0)
        elif e.response['Error']['Code'] == 'InvalidInstanceId.Malformed':
            print(e)
            return e
        else:
            print(e)
            return e
    except Exception as e:
        print(e)
        return e    
    # use waiter
    waiter.wait(InstanceIds=instance_ids)
    # verify response
    return ec2_client.describe_instance_status(InstanceIds=instance_ids)

