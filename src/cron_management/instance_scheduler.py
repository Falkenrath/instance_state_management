from crontab import CronTab
import datetime
import re
from csv import DictReader
from argparse import ArgumentParser
import sys


import src.connections.AWSConnections as awsm
import src.connections.manage_server_state as mss

def get_cli_args(args):
    
    parser = ArgumentParser()
    
    one_of = parser.add_mutually_exclusive_group()
    
    one_of.add_argument("-r", "--run_env_job",
                        action="store_true",
                        help="(Switch) Run env job now")
    
    one_of.add_argument("-n", "--new_job",
                        choices=range(0,25),
                        type=int,
                        metavar= "[0-24]",
                        help= "Set the hour for job to run")
    
    one_of.add_argument("-c", "--clear_environment_jobs",
                        action= "store_true",
                        help="(Switch) Clear out jobs of a specified environment name")

    one_of.add_argument("-m", "--modify_environment_jobs",
                        nargs="?",
                        choices= range(0,25),
                        type=int,
                        metavar= "[0-24]",
                        const=19, 
                        default=None, 
                        help="Modify time to run of a specified environment. Expects an hour at which to run the job")
    one_of.add_argument('-g', "--get_environment_jobs",
                        action="store_true",
                        help="(Switch)Get jobs by environment name"
                        )
    
    parser.add_argument("-s", "--set_state",
                        choices= ['startup', 'shutdown'],
                        help= "Set instances to on or off")
    parser.add_argument("-R", "--instance_region")
    parser.add_argument("-T", "--test", 
                        action='store_true',
                        help="Specify that this run is a test")
    
    requiredNamed = parser.add_argument_group('required named arguments')
    requiredNamed.add_argument('-e', '--environment_name', 
                               choices=['Prod', 'QA', 'Dev', 'Test'], 
                               required=True,
                               help='Name of the environment')
    
    parsed = parser.parse_args(args)
    if parsed.new_job and parsed.instance_region is None:
        parser.error("--new_job requires --instance_region")
    return parsed

# Return jobs from the EnvManagement
def ret_env_job(env_name):
    cron = CronTab( user=True )
    return list(cron.find_comment(f"EnvManagement {env_name}"))
    
    
def ret_env_job_hours(env_name):
    for job in ret_env_job(env_name):
        hour = str(job).split()[1] 
        if hour[0] == '@':
            hour = 0
    
    return int(hour)

        
        

def run_jobs_now(env_name):
    cron = CronTab ( user=True )
    ret_val = ''
    for job in cron.find_comment(f"EnvManagement {env_name}"):
        print(job)
        ret_val += job.run()
    print(ret_val)
    return ret_val
    
    
# Builds and returns a cron job object to shutdown a series of instances
def set_environment_job(env_name, region, startup_shutdown, hour= '19', test=False):
    # Handle no Env variable
    command = f'instancestatemanagement -s {startup_shutdown} -R {region} -e {env_name}'

    cron = CronTab(user=True)
    if test:
        command += " -T"
    job = cron.new(command= command, comment= f"EnvManagement {env_name}")
#
    #default 7pm
    job.setall(f'0 {hour} * * *')
    job.enable()
    assert job.is_valid(), 'Job returned invalid'
    cron.write_to_user( user=True )


# Modifies any jobs with the env_name in the comment
def modify_existing_environment_job_time(env_name, new_hour=19):
    cron = CronTab(user=True)
    for job in cron.find_comment(f"EnvManagement {env_name}"):
        job.setall(f'0 {new_hour} * * *')
    
    cron.write( user=True )


# Remove all environment jobs
def remove_environment_jobs(env_name):
    cron = CronTab(user=True)
    cron.remove_all(comment=f"EnvManagement {env_name}")
    cron.write()
    
    
def handle_state(set_state, env_name, region, test=False):
    # if shutdown or startup
    conn = awsm.EC2Management(region)
    
    conn.get_ids_from_tag('ENV',[env_name])
    
    return_val=None
    if test is False:
        if set_state.lower() == 'startup':
            return_val = mss.start_instances(instance_ids= instances, ec2_client= conn.service_client)
        elif set_state.lower() == 'shutdown':
            return_val = mss.shutdown_instances(instance_ids= instances, ec2_client= conn.service_client)
        else:
            raise
    else:
        if set_state.lower() == 'startup':
            return_val = mss.simulate_start(instance_ids= instances, ec2_client= conn.service_client)
        elif set_state.lower() == 'shutdown':
            return_val = mss.simulate_stop(instance_ids= instances, ec2_client= conn.service_client)
    assert return_val is not None, 'Did not receive status from mss module'
    # Return status code of all instances sent in the request
    return return_val



def main():
    args = get_cli_args(sys.argv[1:])
    instances = []
        
    if args.run_env_job:
        run_jobs_now(args.environment_name)   
    # Set new job
    elif args.new_job is not None:
        set_environment_job(env_name=args.environment_name,
                            hour=args.new_job,
                            startup_shutdown=args.set_state,
                            region=args.instance_region,
                            test=args.test)    
    # If delete env job
    elif args.clear_environment_jobs:
        remove_environment_jobs(args.environment_name)
    # If modify env job
    elif args.modify_environment_jobs:
        modify_existing_environment_job_time(args.environment_name, args.modify_environment_jobs)
    elif args.get_environment_jobs:
        print(ret_env_job(args.environment_name))
    else:
        print(handle_state(set_state=args.set_state, env_name=args.environment_name, region=args.instance_region, test=args.test))
        # Set cronjob back to original state
        modify_existing_environment_job_time(env_name=args.environment_name)

    return 0
    
if __name__ == "__main__":
    main()